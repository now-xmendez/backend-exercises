<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => Str::random(10),
            'price' => rand(0, 300),
            'sku' => Str::random(10),
            'bodega' => Str::random(10),
            'available' => rand(1,15),
            'area' => Str::random(10),
            'state' => true,
            'description' => Str::random(20)
        ]);

        // DB::table('users')->insert([
            // 'name' => 'admin',
            // 'lastname' => 'administrator',
            // 'email' => 'admin@example.com',
            // 'password' => password_hash("12345678",PASSWORD_BCRYPT),
        // ]);

        // DB::table('users')->insert([
        //     'name' => 'operador',
        //     'lastname' => 'operador1',
        //     'email' => 'operador@example.com',
        //     'password' => password_hash("12345678",PASSWORD_BCRYPT),
        // ]);
        // DB::table('users')->insert([
        //     'name' => 'cajero',
        //     'lastname' => 'cajero1',
        //     'email' => 'cajero@example.com',
        //     'password' => password_hash("12345678",PASSWORD_BCRYPT),
        // ]);
    }
}