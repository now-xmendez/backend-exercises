<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Auth;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
     
      // This validation should be on a middleware maybe?

      $user = Auth::user();

      if(!$user->hasRole(['operator'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }

     $products = Product::all();

     return response()->json($products);

    }

     public function create(Request $request)
     {

      $user = Auth::user();
      if(!$user->hasRole(['operator'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }
        $product = new Product;

       $product->name= $request->name;
       $product->price = $request->price;
       $product->description= $request->description;
       $product->sku = $request->sku;
       $product->bodega = $request->bodega;
       $product->available = $request->available;
       $product->area = $request->area;
       $product->state = $request->state;
       
       $product->save();

       return response()->json($product);
     }

     public function show($id)
     {
      $user = Auth::user();
      if(!$user->hasRole(['operator'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }
        $product = Product::find($id);

        return response()->json($product);
     }

     public function update(Request $request, $id)
     {

      $user = Auth::user();
      if(!$user->hasRole(['operator'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }
        $product= Product::find($id);
        
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->save();
        return response()->json($product);
     }

     public function destroy($id)
     {

      $user = Auth::user();
      if(!$user->hasRole(['operator'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }
        $product = Product::find($id);
        $product->delete();

         return response()->json('product removed successfully');
     }


    }

    