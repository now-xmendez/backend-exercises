<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Str;

class UsersController extends Controller

{

  public function __construct()

   {

     //  $this->middleware('auth:api');

   }

   /**

    * Display a listing of the resource.

    *

    * @return \Illuminate\Http\Response

    */

    public function index()
    {
    
      $user = Auth::user();

      if(!$user->hasRole(['admin'])) {
         return response()->json(['error' => 'Forbidden.'], 403);
      }
     
     $users = User::all();

     return response()->json($users);

    }

   public function authenticate(Request $request)

   {

       $this->validate($request, [

       'email' => 'required',

       'password' => 'required'

        ]);

      $user = User::where('email', $request->input('email'))->first();


     if(Hash::check($request->input('password'), $user->password)){

          $apikey = base64_encode(Str::random(40));

          User::where('email', $request->input('email'))->update(['remember_token' => "$apikey"]);;

          return response()->json(['status' => 'success','api_key' => $apikey]);

      }else{

          return response()->json(['status' => 'fail'],401);

      }

   }

}    

?>