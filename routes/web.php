<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


//$router->get('/products', 'ProductController@index')->middleware('role:admin');


$router->group(['prefix'=>'api/v1'], function($app) {
    $app->get('/products', ['middleware' => 'auth', 'uses' => 'ProductController@index']);
    $app->post('/product', ['middleware' => 'auth', 'uses' => 'ProductController@create']);
    $app->get('/product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@show']);
    $app->put('/product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@update']);
    $app->delete('/product/{id}', ['middleware' => 'auth', 'uses' => 'ProductController@destroy']);
    $app->get('/users', ['middleware' => 'auth', 'uses' => 'UsersController@index']);
    $app->post('/login', 'UsersController@authenticate');
    // UsersController@show
    // UsersController@create
    // UsersController@update
    // UsersController@destroy
    $app->get('/sales', ['middleware' => 'auth', 'uses' => 'SalesController@index']);
    $app->post('/sale', ['middleware' => 'auth', 'uses' => 'SalesController@create']);
});