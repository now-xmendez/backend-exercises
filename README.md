# Basic Lumen api with token authetication
This is a basic crud application with token authetincation.

## Set Up

Make sure the .env file is configured properly with your development environment.

For fresh installation follow the next steps.

Install dependencies:

`composer install`

Create migrations:

`php artisan migrate`

Create users, roles and permissions:

`php artisan migrate:fresh --seed --seeder=PermissionsDemoSeeder`

Run the development server:

`php -S localhost:8000 -t public`

## Database Script

The database is located at `scripts/database.sql`

## Endpoints

You can create a new token by using:

```
curl -d '{"email":"admin@example.com", "password":"s3cr3t"}' -H "Content-Type: application/json" -X POST http://localhost:8000/api/v1/login
```

At this moment, there are three tokens in the database:

```
OPERATOR ROLE TOKEN: eHdaVHdlSmVXbFg4TjRWTXdtMExPTDBlSzN3TE0weHlKNUxmbUlmYw==

ADMIN ROLE TOKEN: QXdQWWUySTE3cmFzMnpjaHFCM05EQ2pwazRpak5GQkZaWHR4bGxaMw==

SALES ROLE TOKEN: d3k2d0tCaWVtNjVic0JFSDJScWxYNzJjdjFPbklWclJCM0RPV3p1aw==
```

## Endpoints

The following endpoints are authenticated:

```
OPERATOR:
GET /api/v1/products
POST /api/v1/product
PUT /api/v1/product/{id}
DELETE /api/v1/product/{id}
```
```
ADMIN:
GET /api/v1/users
```

```
SALES
GET /api/v1/sales
POST /api/v1/sale
```

## Requests params

Post a new product:
```
data = {
    'name': 'Some Name',
    'price': 10,
    'sku': 'New Sku',
    'available': 10,
    'bodega': 'a simple description',
    'area': 'An area',
    'state': 1,
    'description': 'a simple description',
    'api_token': operator_token
}
```

Post a new Sale:
```
data = {
    'name': 'Some Name',
    'price': 10,
    'sku': 'New Sku',
    'quantity': 10,
    'description': 'a simple description',
    'api_token': sales_token
}
```

